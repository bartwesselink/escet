//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2021, 2022 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

include::_part_attributes.asciidoc[]

[[rail-commandline]]
== Command-line options

The railroad diagram application has common options shared with many other Esclipse ESCET applications, and several generator options to customize the output.

Non-option values at the command-line are considered to be input files that should be processed by the generator like shown in <<rail-examples>>.

=== General options

[cols="1,1,3", options="header"]
|===
| Option | Default | Description

| `-h` or `--help`
| Disabled
| Get help about how to use the command-line of the railroad application.

| `--show-license`
| Disabled
| Outputs the license of the application.

| `--option-dialog=BOOL`
| `--option-dialog=no`
| Whether to show the option dialog after processing the specified commandline options.

| `-m OUTMODE` or `--output-mode=OUTMODE`
| `--output-mode=normal`
| Amount of output, specify `--output-mode=errors` for getting error output only, `--output-mode=warnings` for getting both errors and warnings as output, `--output-mode=normal` for errors, warnings, and normal output, and `--output-mode=debug` for getting all information that is available.
Note that the latter setting may cause a large amount of output and it may not be understandable unless you know about the internals of the application.

| `--gui=MODE`
| `--gui=auto`
| Whether to use GUI mode.
Use `--gui=off` to disable the GUI entirely which is useful for headless systems, specify `--gui=on` to force-enable the GUI, or `--gui=auto` to use the GUI if one is available.

| `--devmode=BOOL`
| `--devmode=no`
| When enabled with `--devmode=yes`, developer oriented information will be printed when a fault is detected in the program.
Otherwise, the information is written to a file with instructions to send that file to a developer for further analysis.
|===

=== Generator options

The tool currently has several generator options.
You can specify whether output should be written at all, and if so, you can specify the type of output.

[cols="1,1,3", options="header"]
|===
| Option | Default | Description

| `-c CFG` or `--config=CFG`
| Disabled
| The `CFG` value should be a path to a configuration file as explained in the <<rail-customizing-output>> section.
If specified the file contents is used for customizing diagram layout.

| `-f FMT` or `--format=FMT`
| `--format=images`
| The type of output of the program.
Currently `--format=images` produces normal rail diagram `PNG` files, while `--format=debug-images` is only useful for debugging the diagram generator.

| `-w B` or `--write-image=B`
| `--write-image=yes`
| Enable or disable writing the output file, use `--write-image=no` for disabling output.
|===
